from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import *
from .models import *
from django.core import serializers
from datetime import timedelta

# Create your views here.

def index(request):
	context = {}
	template = 'index.html'
	return render(request,template,context)

def handler404(request):
    response = render_to_response('404.html', {},
      context_instance=RequestContext(request))
    response.status_code = 404
    return response

def handler500(request):
    response = render_to_response('500.html', {},
      context_instance=RequestContext(request))
    response.status_code = 500
    return response